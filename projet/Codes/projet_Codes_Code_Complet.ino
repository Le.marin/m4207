#include <LWiFi.h>
#include <LWiFiClient.h>
#include <LWiFiServer.h>
#include <Wire.h>
#include "rgb_lcd.h"
#include <LBattery.h> 

#define WIFI_AP "RT-WIFI-TMP"          // SSID de la WiFi
#define WIFI_PASSWORD "IUTRT97410"  // Mot de passe de la WiFi
#define WIFI_AUTH LWIFI_WPA           // Niveau de sécurité
#define Long_add_MAC 6             //Permet de connaitre la longueur de l'adresse MAC

char buff[256];
byte rtbssid[6];

rgb_lcd lcd;

void setup(){
  
    //BOOT LCD ANIMATION//
    lcd.begin(16, 2);
    lcd.setRGB(150, 0, 0);
    delay(500);
    //END BOOT LCD ANIMATION//
  
    Serial.begin(115200);
    // Initializes LinkIt ONE WiFi module
    LWiFi.begin();

    //Vérification du status de connexion WiFi
    enum LWifiStatus  {
    LWIFI_STATUS_DISCONNECTED,
    LWIFI_STATUS_CONNECTED
    };
    pinMode(4, OUTPUT);

    lcd.setCursor(0, 0);
    lcd.print("Connexion... ");
    Serial.print(WIFI_AP);
    while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD))){
    delay(1000);
  }
}

unsigned long time = 0;

void loop(){

    //==========================================//
    //=============BATTERIE=====================//
    //==========================================//
    
    int Batterylevel = LBattery.level();
    
    lcd.setRGB(150, 150, 150);
    
    lcd.setCursor(0, 0);
    /*if(Batterylevel <= 50){
      L
    }*/
    lcd.print("Batterie: ");
    lcd.print(Batterylevel);
    lcd.print(" %");
    
    delay(100);



    //==========================================//
    //==============LOCALISATION================//
    //==========================================//

    
  //Témoin de connexion
  if(LWIFI_STATUS_CONNECTED){
    String MAC;
    digitalWrite(4, HIGH);
    Serial.println();
    Serial.println("Connexion réussie ! En cours de demande d'information de la carte ... ");
    Serial.print("SSID du WiFi connecté: ");
    Serial.println(LWiFi.SSID());
    IPAddress ip = LWiFi.localIP();
    Serial.print("L'adresse IP: ");
    Serial.println(ip);
    Serial.print("Le masque de l'adresse IP: ");
    Serial.println(LWiFi.subnetMask());

    //Information de l'adresse MAC de la borne WiFi la plus proche
    uint8_t BSSID[Long_add_MAC] = {0};
    LWiFi.BSSID(BSSID);
    //Serial.print("L'adresse MAC de la borne la plus proche est: ");                   //Affichage de l'adresse MAC
    for(int i = 0; i < Long_add_MAC; ++i)
    {
      MAC.concat(BSSID[i]);
      /*Serial.print(BSSID[i], HEX);                                                //Affichage de l'adresse MAC convertie en binaire
      Serial.print(":");*/
    }

    
    lcd.setCursor (0, 1);     
    if(MAC == "1624216871206165"){
      lcd.print("Loc: TD1");
    }
    else if(MAC == "162421687417285"){
      lcd.print("Loc: TD3");
    }
    else if(MAC == "162421687417346"){
      lcd.print("Loc: Reseaux_cablages");
    }
    else if(MAC == "162421687417546"){
      lcd.print("Loc: Signaux_systeme");
    }
    else if(MAC == "162421687417338"){
      lcd.print("Loc: Projet-Doc ");
    }
    else if(MAC == "162421689092104"){
      lcd.print("Loc: Administration");
    }

  //Faire défiler l'écran vers la droite ou la gauche
  for (int positionCounter = 0; positionCounter < 7; positionCounter++) {
    lcd.scrollDisplayLeft();
    delay(50);
    }
  delay(1000);
  for (int positionCounter = 0; positionCounter < 7; positionCounter++) {
    lcd.scrollDisplayRight();
    delay(50);
    }
  delay(4000);

  }
  else{
    digitalWrite(4, LOW);
    digitalWrite(4, HIGH);
    Serial.println("Connexion echouée ...");
  }
}
