#include <LWiFi.h>
#include <LWiFiClient.h>
#include <LWiFiServer.h>

#define WIFI_AP "RT-WIFI-TMP"            // replace your WiFi AP SSID
#define WIFI_PASSWORD "IUTRT97410"  // replace your WiFi AP password
#define WIFI_AUTH LWIFI_WPA           // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your AP
#define Long_add_MAC 6 //Permet de connaitre la longueur de l'adresse MAC

char buff[256];
byte rtbssid[6];

//Scan du réseau WiFi de l'IUT
void rt_ScanAndPrintWiFi(){
  int nu = (int)LWiFi.scanNetworks();
  for(int i=0; i<nu;i++){
    //LWiFi.RTMBSSID(i,(char*)rtbssid);
    sprintf(buff,"BSSID = %X:%X:%X:%X:%X:%X RSSI = %d SSID = %s",rtbssid[0],rtbssid[1],rtbssid[2],rtbssid[3],rtbssid[4],rtbssid[5],LWiFi.RSSI(i),LWiFi.SSID(i));
    Serial.println(buff);
  }
}

void setup(){
  Serial.begin(115200);
  // Initializes LinkIt ONE WiFi module
  LWiFi.begin();

  //Vérification du status de connexion WiFi
  enum LWifiStatus  {
  LWIFI_STATUS_DISABLED,
  LWIFI_STATUS_DISCONNECTED,
  LWIFI_STATUS_CONNECTED
  };
  pinMode(4, OUTPUT);

  Serial.print("Connecting to WiFi AP: ");
  Serial.println(WIFI_AP);
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD))){
    delay(1000);
  }
}

unsigned long time = 0;

void loop(){
  //Scan du réseau WiFI au sein de l'IUT
  rt_ScanAndPrintWiFi();
  
  //Témoin de connexion
  if(LWIFI_STATUS_CONNECTED){
    String MAC;
    digitalWrite(4, HIGH);
    Serial.println();
    Serial.println("Connexion réussie ! En cours de demande d'information de la carte ... ");
    Serial.print("SSID du WiFi connecté: ");
    Serial.println(LWiFi.SSID());
    IPAddress ip = LWiFi.localIP();
    Serial.print("L'adresse IP: ");
    Serial.println(ip);
    Serial.print("Le masque de l'adresse IP: ");
    Serial.println(LWiFi.subnetMask());

    //Information de l'adresse MAC de la borne WiFi la plus proche
    uint8_t BSSID[Long_add_MAC] = {0};
    LWiFi.BSSID(BSSID);
    //Serial.print("L'adresse MAC de la borne la plus proche est: ");
    for(int i = 0; i < Long_add_MAC; ++i)
    {
      MAC.concat(BSSID[i]);
      /*Serial.print(BSSID[i], HEX);
      Serial.print(":");*/
    }
          
    if(MAC == "1624216871206165"){
      Serial.println("Localisation: proche de la salle TD1");
    }
    else if(MAC == "162421687417285"){
      Serial.println("Localisation: proche de la salle TD3");
    }
    else if(MAC == "162421687417346"){
      Serial.println("Localisation: proche de la salle Reseaux et cablages");
    }
    else if(MAC == "162421687417546"){
      Serial.println("Localisation: proche de la salle Signaux et système");
    }
    else if(MAC == "162421687417338"){
      Serial.println("Localisation: proche de la salle Projet et documentation");
    }
    else if(MAC == "162421689092104"){
      Serial.println("Localisation: proche de l'Administration");
    }

    //Saut d'une ligne pour la clarté
    Serial.println();
  }
  else if (LWIFI_STATUS_DISCONNECTED){
    digitalWrite(4, LOW);
    digitalWrite(4, HIGH);
    Serial.println("Connexion echouée ...");
  }

  delay(5000);
}
