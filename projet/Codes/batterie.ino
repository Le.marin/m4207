#include <Wire.h>
#include "rgb_lcd.h"
#include <LBattery.h>

//SET I2C L
rgb_lcd lcd;

//Variable
void setup() {
    //BOOT LCD ANIMATION//
    lcd.begin(16, 2);
    lcd.setRGB(150, 0, 0);
    delay(500);
    //END BOOT LCD ANIMATION//
}

void loop() {

    int Batterylevel = LBattery.level();

   //=========================================//
  // AFFICHAGE EN LCD ..................     //
  //=========================================//

    lcd.setRGB(150, 150, 150);
    
    lcd.setCursor(0, 0);
    lcd.print("Etat: ");
    lcd.print(Batterylevel);
    lcd.print(" %");
    
    delay(100);
    
}


















