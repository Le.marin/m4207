void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(6,OUTPUT);
  pinMode(4,INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (digitalRead(4) == LOW){ /*Le bouton n'est pas appuyé, la lumière reste allumée*/
   digitalWrite(6,HIGH);
   Serial.println("La Led s'allume");
   delay(250);
  }
  else { /*Quand on appuie sur le bouton, la lumière s'éteint*/
   digitalWrite(6,LOW);
   Serial.println("La Led s'éteint");
   delay(250);
  }
}
